<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehiclesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehicles', function(Blueprint $table)
		{
			$table->increments('id');
                      
                        $table->string('trailerType');                       
                        $table->string('license');
                        $table->string('locationStorage');
                        $table->date('deliveryDate');
                        $table->date('pickupDate');
                        $table->date('serviceMoment');
                        $table->boolean('park')->default(false);
                        $table->boolean('pick_up')->default(0)->nullable();
                        $table->text('comments');
                        $table->integer('user_id')->unsigned();
                        $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); 
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehicles');
	}

}

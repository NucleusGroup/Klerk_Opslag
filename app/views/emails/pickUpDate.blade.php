<h1>Pick-up date overdue!</h1>
<p>Client name:  {{$firstname}} {{$lastname}}</p>
<p>Client email: {{$email}}</p>
<p>Client phone: {{$phone}}</p>
<h3>Trailer info:</h3>
<p>Trailer type: {{$trailerType}}</p>
<p>License: {{$license}}</p>
<p>Location storage: {{$locationStorage}}</p>
<p>Pick-up date was on: <strong>{{$date }}</strong></p>


<h1>Rescheduling update for {{$type}}</h1>
<h3>For a client: {{$firstname}} {{$lastname}}</h3>
<p>Email: {{$email}} </p>
<p>Phone: {{$phone}}</p>
<p>Street: {{$street}} </p>
<p>City: {{$city}} - {{$zip}}</p>
<h3>Trailer info:</h3>
<p>Trailer Type: {{$trailerType}} </p>
<p>License: {{$license}} </p>
<p>Location storage: {{$locationStorage}} </p>
<p>New {{$type}}: <strong>{{$date }}</strong></p>


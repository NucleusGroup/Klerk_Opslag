<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8" />
        <title>
            @section('title')
            De Klerk Caravans
            @show
        </title>
        @section('meta_keywords')
        <meta name="keywords" content="de klerk, caravan, trailer,  caravanbedrijf" />
        @show
        @section('meta_author')
        <meta name="author" content="extraordinary" />
        @show
        @section('meta_description')
        <meta name="description" content="Caravan pick up" />
        @show
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
        <link rel="stylesheet" href="{{asset('bootstrap/css/style.css')}}">        
        <link rel="stylesheet" href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css">

        <style>
            body {
                padding: 60px 0;
            }
            @section('styles')
            @show
        </style>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Favicons
        ================================================== -->
        <!--		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
                        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
                        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
                        <link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">-->
        <link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.ico') }}}">
    </head>

    <body>
        <!-- To make sticky footer need to wrap in a div -->
        <div id="wrap">
            <!-- Navbar -->
            <div class="navbar navbar-default navbar-inverse navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav">
                            <li {{ (Request::is('/') ? ' class="active"' : '') }}><a href="{{{ URL::to('') }}}">Home</a></li>
                        </ul>

                        <ul class="nav navbar-nav pull-right">
                            @if (Auth::check())
                            @if (Auth::user()->hasRole('admin'))
                            <li><a href="{{{ URL::to('admin/users') }}}">Admin Panel</a></li>
                            @endif
                            <li><a href="{{{ URL::to('user') }}}">Logged in as {{{ Auth::user()->firstName }}}</a></li>
                            <li><a href="{{{ URL::to('user/logout') }}}">Logout</a></li>
                            @else
                            <li><a href="{{{ URL::to('user/login/admin') }}}">Admin</a></li>

                            @endif
                        </ul>
                        <!-- ./ nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- ./ navbar -->

            <!-- Container -->
            <div class="container">
                <!-- Notifications -->
                @include('notifications')
                <!-- ./ notifications -->

                <!-- Content -->
                @yield('content')
                <!-- ./ content -->
            </div>
            <!-- ./ container -->

            <!-- the following div is needed to make a sticky footer -->
            <div id="push"></div>
        </div>
        <!-- ./wrap -->


        <!--<div id="footer">
          <div class="container">
            <p class="muted credit">Laravel 4 Starter Site on <a href="https://github.com/andrew13/Laravel-4-Bootstrap-Starter-Site">Github</a>.</p>
          </div>
        </div> -->

        <!-- Javascripts
        ================================================== -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>

        <script src="{{asset('assets/js/Moment.js')}}"></script>
        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
        <script src="{{asset('assets/js/deleteConf.js')}}"></script>
        <script type="text/javascript">


<?php
if (isset($vehicles)) {
    foreach ($vehicles as $key => $value) {
        ?>
        $('#serviceMomentUser<?php echo $key + 1 ?>').datetimepicker({minDate: moment(), format: 'DD/MM/YYYY'});
        $('#pickupDateUser<?php echo $key + 1 ?>').datetimepicker({minDate: moment(), format: 'DD/MM/YYYY'});

    <?php }
} ?>
        </script>

        @yield('scripts')
    </body>
</html>

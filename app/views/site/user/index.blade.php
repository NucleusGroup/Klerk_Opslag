@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.account') }}} ::
@parent
@stop

{{-- New Laravel 4 Feature in use --}}
@section('styles')
@parent
body {
background: #fff;
}
@stop

{{-- Content --}}
@section('content')

<div class="page-header">
    <img src="{{asset('assets/img/logo.jpg')}}" width="350" style="margin-bottom: 20px" alt="logo"/>
    <h2>Voertuig informatie</h2>
</div>
<div class="row">
    <div class="col-md-6">

        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label">Voornaam:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $user->firstName }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Achternaam:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $user->lastName }}</p>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-4 control-label">Plaats:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $user->city }}</p>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-4 control-label">Straat:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $user->street }}</p>
                </div>
            </div>

        </form>  

    </div>
    <div class="col-md-6">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-4 control-label">Postcode:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $user->zip }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Email:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $user->email }}</p>
                </div>
            </div>    
            <div class="form-group">
                <label class="col-sm-4 control-label">Telefoonnummer:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $user->phone }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Mobile:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $user->mobile }}</p>
                </div>
            </div>
        </form>
    </div>
</div>

@foreach($vehicles as $key => $vehicle)
<?php
    if(isset($vehicle)) {
        $deliveryDate = date_create($vehicle->deliveryDate);         
        $pickupDate = date_create($vehicle->pickupDate); 
    }
    ?>
<hr>
<div class="row top_pad">
<div class="col-md-6 line">
    <form class="form-horizontal" action="{{action('UserController@postDate')}}" method="post">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{{ $vehicle->id}}" />
        <!-- ./ csrf token -->
        <div class="form-group">
            <label class="col-sm-4 control-label">Unit number:</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{  $vehicle->id }}</p>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-4 control-label">Type caravan:</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{  $vehicle->trailerType }}</p>
            </div>
        </div>
        
        

        <div class="form-group">
            <label class="col-sm-4 control-label">Opslag locatie:</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{  $vehicle->locationStorage }}</p>
            </div>
        </div>
        <div class="form-group">
                <label class="col-sm-4 control-label">Vergunning:</label>
                <div class="col-sm-8">
                    <p class="form-control-static">{{  $vehicle->license }}</p>
                </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Bezorg datum:</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{  date_format($deliveryDate, 'd/m/Y') }}</p>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Ophaal datum:</label>
            <div class="col-sm-8">
                <p class="form-control-static">{{  date_format($pickupDate, 'd/m/Y') }}</p>
            </div>
        </div>

        <!-- service moment -->

        <div class="form-group ">
            <label class="col-md-4 control-label" for="serviceMomentUser">Veiligheidscontrole/<br/>Oonderhoudsbeurt</label>
            <div  class="col-md-4 input-group"  id="serviceMomentUser{{ $key + 1 }}">
                <input  type='text' required  id="serviceMomentUser{{ $key +1 }}"  name="serviceMomentUser"  class="form-control" value="" />
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>            
                </span> 

            </div>
            <button type="submit" id="confirmService" class="btn btn-success">Bevestigen</button>   
        </div>



        <!-- ./ service moment -->

    </form> 

</div>
<div class="col-md-6">
    <form action="{{action('UserController@postDate')}}" method="post">
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{{ $vehicle->id}}" />
        <!-- ./ csrf token -->
        
        <div class="form-group col-md-8">
            <label class=" control-label" style="padding-left: 0;" for="pickupDateUser">Wilt u de ophaaldatum wijzigen?</label>
        </div>
        
                  
            <div class="col-md-5">
                <div  class="input-group"  id="pickupDateUser{{ $key + 1 }}">
                    <input type='text'  id="pickupDateUser{{ $key + 1 }}"  name="pickupDateUser" class="form-control" value="" required />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>            
                    </span>
                </div>
                <button type="submit" style="margin-top: 20px"  class="btn btn-success" >Bevestigen</button>
            </div>
           
       
    </form>
</div>
    
</div>

@endforeach
@stop

@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	<!-- Tabs -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
		</ul>
	<!-- ./ tabs -->

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($user)){{ URL::to('admin/vehicles/' . $vehicle->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                            
                                <!-- firstName -->
                                <div class="form-group {{{ $errors->has('firstName') ? 'error' : '' }}}">
                                    <label class="col-md-2 control-label" for="firstName">First Name</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="firstName" id="firstName" value="{{{ Input::old('firstName', isset($vehicle) ? $vehicle->firstName : null) }}}" />
                                        {{ $errors->first('firstName', '<span class="help-inline">:message</span>') }}
                                    </div>
                                </div>
                                <!-- ./ firstName -->

                                <!-- lastName -->
                                <div class="form-group {{{ $errors->has('lastName') ? 'error' : '' }}}">
                                    <label class="col-md-2 control-label" for="lastName">Last Name</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" name="lastName" id="lastName" value="{{{ Input::old('lastName', isset($vehicle) ? $vehicle->lastName : null) }}}" />
                                        {{ $errors->first('lastName', '<span class="help-inline">:message</span>') }}
                                    </div>
                                </div>
                                <!-- ./ lastName -->

                                
        
				<!-- username -->
				<div class="form-group {{{ $errors->has('username') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="username">Username</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="username" id="username" value="{{{ Input::old('username', isset($vehicle) ? $vehicle->username : null) }}}" />
						{{ $errors->first('username', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ username -->

				

				

				
				

				
			</div>
			<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<a class="btn btn-default btn-small btn-inverse" href="{{ URL::to('admin/vehicles') }}">Cancel</a>
				
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
